.. Plom documentation
   Copyright (C) 2022-2023 Colin B. Macdonald
   SPDX-License-Identifier: AGPL-3.0-or-later

``plom.client`` module
----------------------

Plom Client
^^^^^^^^^^^

.. automodule:: plom.client
    :members:
.. automodule:: plom.client.annotator
    :members:
.. automodule:: plom.client.downloader
    :members:


.. _module-plom-manager:

``plom.manager`` module
^^^^^^^^^^^^^^^^^^^^^^^

.. automodule:: plom.manager
    :members:
