.. Plom documentation
   Copyright (C) 2022-2023 Colin B. Macdonald
   SPDX-License-Identifier: AGPL-3.0-or-later

``plom-manager``
----------------

.. argparse::
   :ref: plom.manager.__main__.get_parser
   :prog: plom-manager
